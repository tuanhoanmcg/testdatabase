﻿using DataBaseTest.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Data
{
    public class DataBaseTestContext:DbContext
    {
        public DataBaseTestContext(DbContextOptions<DataBaseTestContext> options) : base(options)
        { }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; } 
        public DbSet<ScheduleCustomer> ScheduleCustomers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("pk_Employee"); 
            });
            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("pk_Project"); 
            });
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id)
                      .HasName("pk_Customer");

                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP"); 

                entity.HasOne(c => c.CreatorNavigation)
                    .WithMany(c => c.Customers)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Customer_Employee");
                entity.HasOne(e => e.ProjectNavigation)
                    .WithMany(e => e.Customers)
                    .HasForeignKey(e => e.ProjectInterestedId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Customer_Project");
            });
            modelBuilder.Entity<ScheduleCustomer>(entity =>
            {
                entity.HasKey(e => new { e.CreatorId, e.TimeStart, e.CustomerId }); 

                entity.HasOne(e => e.CustomerNavigation)
                    .WithMany(e => e.ScheduleCustomers)
                    .HasForeignKey(e => e.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Customer");
                entity.HasOne(e => e.ProjectNavigation)
                    .WithMany(e => e.ScheduleCustomers)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Project");
                entity.HasOne(e => e.EmployeeNavigation)
                    .WithMany(e => e.ScheduleCustomers)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Creator");
            });
            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Id); 

                entity.HasOne(e => e.CustomerNavigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Order_Customer");
                entity.HasOne(e => e.EmployeeNavigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Order_Employee");
                entity.HasOne(e => e.ProjectNavvigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Order_Project");
            });
            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.OrderId, e.EmployeeId });
                 
                entity.HasOne(e => e.EmployeeNavigation)
                .WithMany(e => e.OrderDetails)
                    .HasForeignKey(e => e.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_OrderDetail_Employee");
                entity.HasOne(e => e.OrderNavigation)
                .WithMany(e => e.OrderDetails)
                    .HasForeignKey(e => e.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_OrderDetail_Order");
            });
        }
    }
}
