﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Models
{
    public class Employee
    {
        public Employee()
        {
            Customers = new HashSet<Customer>();
            ScheduleCustomers = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
            OrderDetails = new HashSet<OrderDetail>(); 
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [MaxLength(200)]
        public string Name { get; set; }
        [Column("email")]
        [MaxLength(50)]
        public string Email { get; set; }

        public virtual ICollection<Customer> Customers { get; set; } 
        public virtual ICollection<ScheduleCustomer> ScheduleCustomers { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
