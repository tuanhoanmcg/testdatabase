﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Models
{
    public class Customer
    {
        public Customer()
        { 
            ScheduleCustomers = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("phone_number")]
        [MaxLength(15)]
        public string PhoneNumber { get; set; }
        [Column("name")]
        [MaxLength(200)]
        public string Name { get; set; }
        [Column("need")]
        [MaxLength(10)]
        public string Need { get; set; } //Mua hay Thue.
        [MaxLength(1000)]
        [Column("description")]
        public string Description { get; set; }//Mo Ta nhu cau cua khach.
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("creator_id")]
        public int CreatorId { get; set; }
        [Column("project_interested")]
        public int ProjectInterestedId { get; set; }
        public string Status { get; set; } // Moi quan he
        public virtual Project ProjectNavigation { get; set; }
        public virtual Employee CreatorNavigation { get; set; } 
        public virtual ICollection<ScheduleCustomer> ScheduleCustomers { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
