﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Models
{
    public class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("fee_total")]
        public double FeeTotal { get; set; }
        [Column("fee_received")]
        public double FeeReceived { get; set; }
        [Column("create_at")]
        public DateTime CreateAt { get; set; }
        [Column("contract_type")]
        [MaxLength(20)]
        public string ContractType { get; set; } //Loại hợp đồng
        [Column("creator_id")]
        public int CreatorId { get; set; } //Id người tạo đơn hàng  
        [Column("customer_id")]
        public int CustomerId { get; set; }
        [Column("project_id")]
        public int ProjectId { get; set; }
        public virtual Customer CustomerNavigation { get; set; } 
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Project ProjectNavvigation { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    } 
}
