﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataBaseTest.Models
{
    public class Project
    {
        public Project()
        {
            Customers = new HashSet<Customer>();
            ScheduleCustomers = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [MaxLength(200)]
        public string Name { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<ScheduleCustomer> ScheduleCustomers { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
