﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Models
{
    public class ScheduleCustomer
    { 
        [Column("creator_id")]
        public int CreatorId { get; set; } //Id người tạo lịch hẹn   
        [Column("customer_id")]
        public int CustomerId { get; set; } 
        [Column("time_start")]
        public DateTime TimeStart { get; set; }
        [Column("time_end")]
        public DateTime TimeEnd { get; set; }
        [Column("status_schedule",TypeName ="jsonb")]
        public List<StatusSchedule> StatusSchedule { get; set; } //Trạng thái lịch hẹn 
        [Column("project_id")]
        public int ProjectId { get; set; } 
        [Column("authorized_person_id", TypeName = "jsonb")]
        public List<int> AssignedEmployees { get; set; } //Id người được ủy quyền
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Project ProjectNavigation { get; set; } 
}
    public class StatusSchedule
    {
        public DateTime CreateAt { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; } 
    } 
}
