﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataBaseTest.Models
{
    public class OrderDetail
    {
        [Column("employee_id")]
        public int EmployeeId { get; set; } 
        [Column("order_id")]
        public int OrderId { get; set; } 
        [Column("percent_contribute")]
        public double PercentContribute { get; set; } //% đóng góp trong đơn
        [Column("job_type")]
        [MaxLength(200)]
        public string JobType { get; set; } //Loại công việc 
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Order OrderNavigation { get; set; }
    }
}
