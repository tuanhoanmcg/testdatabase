﻿// <auto-generated />
using System;
using System.Collections.Generic;
using DataBaseTest.Data;
using DataBaseTest.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DataBaseTest.Migrations
{
    [DbContext(typeof(DataBaseTestContext))]
    partial class DataBaseTestContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("DataBaseTest.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("created_at")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<int>("CreatorId")
                        .HasColumnType("integer")
                        .HasColumnName("creator_id");

                    b.Property<string>("Description")
                        .HasMaxLength(1000)
                        .HasColumnType("character varying(1000)")
                        .HasColumnName("description");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("character varying(200)")
                        .HasColumnName("name");

                    b.Property<string>("Need")
                        .HasMaxLength(10)
                        .HasColumnType("character varying(10)")
                        .HasColumnName("need");

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(15)
                        .HasColumnType("character varying(15)")
                        .HasColumnName("phone_number");

                    b.Property<int>("ProjectInterestedId")
                        .HasColumnType("integer")
                        .HasColumnName("project_interested");

                    b.Property<string>("Status")
                        .HasColumnType("text");

                    b.HasKey("Id")
                        .HasName("pk_Customer");

                    b.HasIndex("CreatorId");

                    b.HasIndex("ProjectInterestedId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("DataBaseTest.Models.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Email")
                        .HasMaxLength(50)
                        .HasColumnType("character varying(50)")
                        .HasColumnName("email");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("character varying(200)")
                        .HasColumnName("name");

                    b.HasKey("Id")
                        .HasName("pk_Employee");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("DataBaseTest.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("ContractType")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("contract_type");

                    b.Property<DateTime>("CreateAt")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("create_at");

                    b.Property<int>("CreatorId")
                        .HasColumnType("integer")
                        .HasColumnName("creator_id");

                    b.Property<int>("CustomerId")
                        .HasColumnType("integer")
                        .HasColumnName("customer_id");

                    b.Property<double>("FeeReceived")
                        .HasColumnType("double precision")
                        .HasColumnName("fee_received");

                    b.Property<double>("FeeTotal")
                        .HasColumnType("double precision")
                        .HasColumnName("fee_total");

                    b.Property<int>("ProjectId")
                        .HasColumnType("integer")
                        .HasColumnName("project_id");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("DataBaseTest.Models.OrderDetail", b =>
                {
                    b.Property<int>("OrderId")
                        .HasColumnType("integer")
                        .HasColumnName("order_id");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("integer")
                        .HasColumnName("employee_id");

                    b.Property<string>("JobType")
                        .HasMaxLength(200)
                        .HasColumnType("character varying(200)")
                        .HasColumnName("job_type");

                    b.Property<double>("PercentContribute")
                        .HasColumnType("double precision")
                        .HasColumnName("percent_contribute");

                    b.HasKey("OrderId", "EmployeeId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("OrderDetails");
                });

            modelBuilder.Entity("DataBaseTest.Models.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("character varying(200)")
                        .HasColumnName("name");

                    b.HasKey("Id")
                        .HasName("pk_Project");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("DataBaseTest.Models.ScheduleCustomer", b =>
                {
                    b.Property<int>("CreatorId")
                        .HasColumnType("integer")
                        .HasColumnName("creator_id");

                    b.Property<DateTime>("TimeStart")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("time_start");

                    b.Property<int>("CustomerId")
                        .HasColumnType("integer")
                        .HasColumnName("customer_id");

                    b.Property<List<int>>("AssignedEmployees")
                        .HasColumnType("jsonb")
                        .HasColumnName("authorized_person_id");

                    b.Property<int>("ProjectId")
                        .HasColumnType("integer")
                        .HasColumnName("project_id");

                    b.Property<List<StatusSchedule>>("StatusSchedule")
                        .HasColumnType("jsonb")
                        .HasColumnName("status_schedule");

                    b.Property<DateTime>("TimeEnd")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("time_end");

                    b.HasKey("CreatorId", "TimeStart", "CustomerId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("ScheduleCustomers");
                });

            modelBuilder.Entity("DataBaseTest.Models.Customer", b =>
                {
                    b.HasOne("DataBaseTest.Models.Employee", "CreatorNavigation")
                        .WithMany("Customers")
                        .HasForeignKey("CreatorId")
                        .HasConstraintName("fk_Customer_Employee")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Project", "ProjectNavigation")
                        .WithMany("Customers")
                        .HasForeignKey("ProjectInterestedId")
                        .HasConstraintName("fk_Customer_Project")
                        .IsRequired();

                    b.Navigation("CreatorNavigation");

                    b.Navigation("ProjectNavigation");
                });

            modelBuilder.Entity("DataBaseTest.Models.Order", b =>
                {
                    b.HasOne("DataBaseTest.Models.Employee", "EmployeeNavigation")
                        .WithMany("Orders")
                        .HasForeignKey("CreatorId")
                        .HasConstraintName("fk_Order_Employee")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Customer", "CustomerNavigation")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId")
                        .HasConstraintName("fk_Order_Customer")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Project", "ProjectNavvigation")
                        .WithMany("Orders")
                        .HasForeignKey("ProjectId")
                        .HasConstraintName("fk_Order_Project")
                        .IsRequired();

                    b.Navigation("CustomerNavigation");

                    b.Navigation("EmployeeNavigation");

                    b.Navigation("ProjectNavvigation");
                });

            modelBuilder.Entity("DataBaseTest.Models.OrderDetail", b =>
                {
                    b.HasOne("DataBaseTest.Models.Employee", "EmployeeNavigation")
                        .WithMany("OrderDetails")
                        .HasForeignKey("EmployeeId")
                        .HasConstraintName("fk_OrderDetail_Employee")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Order", "OrderNavigation")
                        .WithMany("OrderDetails")
                        .HasForeignKey("OrderId")
                        .HasConstraintName("fk_OrderDetail_Order")
                        .IsRequired();

                    b.Navigation("EmployeeNavigation");

                    b.Navigation("OrderNavigation");
                });

            modelBuilder.Entity("DataBaseTest.Models.ScheduleCustomer", b =>
                {
                    b.HasOne("DataBaseTest.Models.Employee", "EmployeeNavigation")
                        .WithMany("ScheduleCustomers")
                        .HasForeignKey("CreatorId")
                        .HasConstraintName("fk_ScheduleCustomer_Creator")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Customer", "CustomerNavigation")
                        .WithMany("ScheduleCustomers")
                        .HasForeignKey("CustomerId")
                        .HasConstraintName("fk_ScheduleCustomer_Customer")
                        .IsRequired();

                    b.HasOne("DataBaseTest.Models.Project", "ProjectNavigation")
                        .WithMany("ScheduleCustomers")
                        .HasForeignKey("ProjectId")
                        .HasConstraintName("fk_ScheduleCustomer_Project")
                        .IsRequired();

                    b.Navigation("CustomerNavigation");

                    b.Navigation("EmployeeNavigation");

                    b.Navigation("ProjectNavigation");
                });

            modelBuilder.Entity("DataBaseTest.Models.Customer", b =>
                {
                    b.Navigation("Orders");

                    b.Navigation("ScheduleCustomers");
                });

            modelBuilder.Entity("DataBaseTest.Models.Employee", b =>
                {
                    b.Navigation("Customers");

                    b.Navigation("OrderDetails");

                    b.Navigation("Orders");

                    b.Navigation("ScheduleCustomers");
                });

            modelBuilder.Entity("DataBaseTest.Models.Order", b =>
                {
                    b.Navigation("OrderDetails");
                });

            modelBuilder.Entity("DataBaseTest.Models.Project", b =>
                {
                    b.Navigation("Customers");

                    b.Navigation("Orders");

                    b.Navigation("ScheduleCustomers");
                });
#pragma warning restore 612, 618
        }
    }
}
